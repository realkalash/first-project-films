package com.example.retrofit.database;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.retrofit.model.FilmsModel;


@Database(entities = {FilmsModel.class}, version = 1) /* Это наша база данных, хранящая модельки FilmsModel и версии 1*/
public abstract class MyDataBase extends RoomDatabase {

    public abstract MyDataBaseDao getFilmsDao();

}
