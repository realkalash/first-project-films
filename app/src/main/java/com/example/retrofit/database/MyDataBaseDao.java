package com.example.retrofit.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.retrofit.model.FilmsModel;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class MyDataBaseDao {

    @Insert
    public abstract void insertAll(List<FilmsModel> filmsModels);

    @Query("SELECT * FROM FilmsModel")
    public abstract Flowable<List<FilmsModel>> selectAll();


    @Query("SELECT COUNT(*) FROM FilmsModel")
    public abstract Integer countAll(); /* Возвращает кол-во элементов в БД */

    @Query("DELETE FROM FilmsModel")
    public abstract void removeAll();

}
