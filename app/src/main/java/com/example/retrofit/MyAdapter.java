package com.example.retrofit;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.retrofit.model.FilmsModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    List<FilmsModel> filmsModels;
    Context context;
    OnFilmClickListener listener;

    public MyAdapter(List<FilmsModel> filmsModels, Context context, OnFilmClickListener listener) {
        this.filmsModels = filmsModels;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return filmsModels.size();
    }

    public interface OnFilmClickListener {
        void onItemClick(FilmsModel filmsModel);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.imageView2)
        ImageView photo;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final int position) {
            name.setText(filmsModels.get(position).getNameFilm());

            Glide.with(context)
                    .load(filmsModels.get(position).getPhotoURL())
                    .error(new ColorDrawable(Color.RED))
//                    .placeholder(R.drawable.loading)
                    .into(photo);
//
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    listener.onItemClick(filmsModelList.get(position));
//                }
//            });
        }
    }
}