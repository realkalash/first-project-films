package com.example.retrofit.model;


import androidx.room.Entity;

@Entity /*Можем запихнуть в БД*/
public class FilmsModel {

    String nameFilm;
    String ganreFilm;
    String yearFilm;
    String photoURL;


    public FilmsModel(String nameFilm, String ganreFilm, String yearFilm, String photoURL) {
        this.nameFilm = nameFilm;
        this.ganreFilm = ganreFilm;
        this.yearFilm = yearFilm;
        this.photoURL = photoURL;
    }

    public FilmsModel(String nameFilm, String ganreFilm, String yearFilm) {
        this.nameFilm = nameFilm;
        this.ganreFilm = ganreFilm;
        this.yearFilm = yearFilm;
    }

    public String getNameFilm() {
        return nameFilm;
    }

    public void setNameFilm(String nameFilm) {
        this.nameFilm = nameFilm;
    }

    public String getGanreFilm() {
        return ganreFilm;
    }

    public void setGanreFilm(String ganreFilm) {
        this.ganreFilm = ganreFilm;
    }

    public String getYearFilm() {
        return yearFilm;
    }

    public void setYearFilm(String yearFilm) {
        this.yearFilm = yearFilm;
    }

    public String getPhotoURL() {
        return photoURL;
    }
}
