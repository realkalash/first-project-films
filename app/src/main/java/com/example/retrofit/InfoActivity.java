package com.example.retrofit;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Intent.EXTRA_TITLE;


public class InfoActivity extends AppCompatActivity {

    @BindView(R.id.ia_descr)
    TextView descr;

    @BindView(R.id.ia_name)
    TextView name;

    @BindView(R.id.ia_photo)
    ImageView photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        name.setText(getIntent().getStringExtra(EXTRA_TITLE));
        descr.setText(getIntent().getStringExtra(MainActivity.EXTRA_DES));
        Glide.with(this).load(getIntent().getStringExtra(MainActivity.EXTRA_PHOTO)).into(photo);

    }


}
