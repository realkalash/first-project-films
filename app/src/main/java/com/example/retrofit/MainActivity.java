package com.example.retrofit;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.example.retrofit.database.MyDataBase;
import com.example.retrofit.model.FilmsModel;
import com.example.retrofit.model.ResponseModel;
import com.example.retrofit.utils.ApiService;
import com.example.retrofit.utils.Converter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static android.content.Intent.EXTRA_TITLE;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnFilmClickListener {

    public static final String EXTRA_PHOTO = "photo";
    public static final String EXTRA_YEAR = "year";
    public static final String EXTRA_DES = "ganre";
    List<FilmsModel> filmsModelList = new ArrayList<>();

    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);

        /*ВАЖНО ROOM.DATABASEBUILDER и название БД вашего класса*/
        final MyDataBase myDataBase = Room.databaseBuilder(this, MyDataBase.class, "database")
                .allowMainThreadQueries() // разрешаем выполнять операции в основном потоке
                .build();


        myDataBase.getFilmsDao().selectAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<FilmsModel>>() {
                    @Override
                    public void accept(List<FilmsModel> filmsModels) throws Exception {
                        filmsModelList = filmsModels;

                    }
                });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        MyAdapter myAdapter = new MyAdapter(filmsModelList, MainActivity.this, MainActivity.this);

        int count = myDataBase.getFilmsDao().countAll();

        if (count > 0)
        {
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(myAdapter);
        }
        else
            RequestToServer(myDataBase);


    }

    private void RequestToServer(final MyDataBase myDataBase) {
        ApiService.getAll("f335b4a37f3cccbcb277de8048581bb8", "1")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<ResponseModel>() {
                               @Override
                               public void accept(ResponseModel responseModel) throws Exception {

                                   filmsModelList = Converter.convertRequest(responseModel);

                                   recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

                                   recyclerView.setAdapter(new MyAdapter(filmsModelList, MainActivity.this, MainActivity.this));

                                   myDataBase.getFilmsDao().insertAll(filmsModelList);

                               }
                           }

                );
    }

    @Override
    public void onItemClick(FilmsModel filmsModel) {
        Intent intent = new Intent(this, InfoActivity.class);
        intent.putExtra(EXTRA_TITLE, filmsModel.getNameFilm());
        intent.putExtra(EXTRA_PHOTO, filmsModel.getPhotoURL());
        intent.putExtra(EXTRA_DES, filmsModel.getGanreFilm());
        intent.putExtra(EXTRA_YEAR, filmsModel.getYearFilm());

        startActivity(intent);
    }
}
