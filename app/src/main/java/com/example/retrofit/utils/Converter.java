package com.example.retrofit.utils;

import com.example.retrofit.model.FilmsModel;
import com.example.retrofit.model.ResponseModel;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public static List<FilmsModel> convertRequest(ResponseModel responseModel) {

        List<FilmsModel> films = new ArrayList<>();

        List<ResponseModel.Result> results = responseModel.getResults();

        for (int i = 0; i < results.size(); i++) {

            ResponseModel.Result filmModel = results.get(i);
            films.add(new FilmsModel(filmModel.getTitle(),
                    filmModel.getOverview(),
                    filmModel.getReleaseDate(),
                    filmModel.getPosterPath()));

        }
        return films;
    }

}
