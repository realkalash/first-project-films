package com.example.retrofit.utils;



import com.example.retrofit.model.ResponseModel;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ApiService {
    private static final String API = "https://api.themoviedb.org/3/";
    public static String apiKey = "f335b4a37f3cccbcb277de8048581bb8";
    private static PrivateApi privateApi;

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);

    }


    public static Observable<ResponseModel> getAll(String apiKey, String page) {
        return privateApi.getFilmsPosters(apiKey, page);
    }

    public interface PrivateApi {
        @GET("movie/popular")
        Observable<ResponseModel> getFilmsPosters(@Query("api_key") String apiKey,@Query("page") String page);
    }

}
